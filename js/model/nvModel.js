function NhanVien(
  _taiKhoan,
  _ten,
  _email,
  _matKhau,
  _ngayLam,
  _luongCoBan,
  _chucVu,
  _gioLam
) {
  this.taiKhoan = _taiKhoan;
  this.ten = _ten;
  this.email = _email;
  this.matKhau = _matKhau;
  this.ngayLam = _ngayLam;
  this.luongCoBan = _luongCoBan;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;
  this.xepLoai = function () {
     this.loai = ""; 
    if (~~this.gioLam >= 192) {
      return this.loai ="Xuất Sắc";
    } else if (~~this.gioLam >= 176) {
      return this.loai ="Giỏi";
    } else if (~~this.gioLam >= 160) {
      return this.loai ="Khá";
    } else {
      return  this.loai ="Trung Bình";
    }
  };

  this.tongLuong = function () {
    if (this.chucVu == "Sếp") {
      return ~~this.luongCoBan * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      return ~~this.luongCoBan * 2;
    } else {
      return (~~this.luongCoBan);
    }
  };
}
