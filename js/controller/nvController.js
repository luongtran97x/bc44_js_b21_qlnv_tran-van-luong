function render(dsnv) {
  var contentHTML = "";
  for (var i = 0; i < dsnv.length; i++) {
    var nv = dsnv[i];
    var contentTr = ` 
        <tr>
            <td>${nv.taiKhoan}</td> 
            <td>${nv.ten}</td> 
            <td>${nv.email}</td>
            <td>${nv.ngayLam}</td> 
            <td>${nv.chucVu}</td> 
            <td>${nv.tongLuong().toLocaleString()}VND</td>  
            
            <td>${nv.xepLoai()}</td>
            <td> 
                <button onclick="btnXoa(${
                  nv.taiKhoan
                })" class="btn btn-danger">Delete<button>  
                <button onclick="btnSua(${
                  nv.taiKhoan
                })" class="btn btn-success " data-target="#myModal" data-toggle="modal">Edit<button>  
            </td>  
        </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function layThongTinTuForm() {
  var taiKhoan = document.getElementById("tknv").value;
  var ten = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCoBan = document.getElementById("luongCB").value;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;
  // lưu thông tin vào mảng
  var nv = new NhanVien(
    taiKhoan,
    ten,
    email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLam
  );
  return nv;
}

function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luongCoBan*1 ;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam *1;
};

