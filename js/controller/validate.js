var showMessage = function (id, message) {
  document.getElementById(id).innerHTML = message;
};
var isEmpty = function (id, value) {
  if (value.length == 0) {
    showMessage(id, "Trường này không được bỏ trống");
    return false;
  } else {
    showMessage(id, "");
    return true;
  }
};
var isLength = function (value, id) {
  if (value.length >= 4 && value.length <= 6) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Nhập từ 4 - 6 chữ số");
    return false;
  }
};
var isNumber = function (value, id) {
  if (isNaN(value)) {
    showMessage(id, "Chỉ nhập vào số");
  } else {
    showMessage(id, "");
    return true;
  }
};
var isLetters = function (id, value) {
  if (value.match(/^[a-zA-Z\s]+$/)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Vui Lòng chỉ nhập chữ");
    return false;
  }
};
var isDulicate = function (id, nvArr) {
  var index = nvArr.findIndex(function (item) {
    return id == item.taiKhoan;
  });
  if (index == -1) {
    showMessage("tbTKNV", "");
    return true;
  } else {
    showMessage("tbTKNV", "Tài khoản bị trùng ");
    return false;
  }
};
var isEmail = function (id, value) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(value)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Vui lòng nhập theo định dạng abcd@domain.com");
    return false;
  }
};
var isPassword = function (value, id) {
  const re = /^(?=.*[0-9])(?=.*[A-Z])(?=.*\W)(?!.* ).{6,10}$/;
  if (re.test(value)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(
      id,
      "Mật khẩu phải chứa từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt"
    );
    return false;
  }
};
var isDate = function (id, value) {
  const date_regex =
    /^((0?[1-9]|1[012])[/](0?[1-9]|[12][0-9]|3[01])[/](19|20)?[0-9]{2})*$/;
  if (date_regex.test(value)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Vui lòng nhập theo thứ tự tháng / ngày / năm");
    return false;
  }
};
var salary = function (id, value) {
  if (value >= 1000000 && value <= 20000000) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Nhập lương cơ bản từ 1.000.000 -> 20.000.000");
    return false;
  }
};
var isPosition = function (id, value) {
  if (value == 0) {
    showMessage(id, "Vui lòng chọn chức vụ");
    return false;
  } else {
    showMessage(id, "");
    return true;
  }
};
var workTime = function (id, value) {
  if (value >= 80 && value <= 200) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(
      id,
      "Sô giờ làm trong tháng ít nhất 80h và không vượt quá 200h"
    );
    return false;
  }
};
validate = function (nv) {
  // validate tài khoản
  var isValid =
    isEmpty("tbTKNV", nv.taiKhoan) &&
    isLength(nv.taiKhoan, "tbTKNV") &&
    isNumber(nv.taiKhoan, "tbTKNV"); 
    // isDulicate(nv.taiKhoan, nvArr);
  // validate tên
  isValid = isValid & isEmpty("tbTen", nv.ten) && isLetters("tbTen", nv.ten);
  // validate  email
  isValid =
    isValid & isEmpty("tbEmail", nv.email) && isEmail("tbEmail", nv.email);
  // validate password
  isValid =
    isValid & isEmpty("tbMatKhau", nv.matKhau) &&
    isPassword(nv.matKhau, "tbMatKhau");
  // vailidate ngày làm
  isValid =
    isValid & isEmpty("tbNgay", nv.ngayLam) && isDate("tbNgay", nv.ngayLam);
  // validate lương cơ bản
  isValid =
    isValid & isEmpty("tbLuongCB", nv.luongCoBan) &&
    salary("tbLuongCB", nv.luongCoBan);
  // validate chức vụ
  isValid = isValid & isPosition("tbChucVu", nv.chucVu);
  // validate thời gian làm việc
  isValid =
    isValid & isEmpty("tbGiolam", nv.gioLam) && workTime("tbGiolam", nv.gioLam);
  return isValid;
};
