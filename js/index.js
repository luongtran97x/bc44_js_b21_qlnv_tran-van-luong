var nvArr = [];
// lấy dữ liệu lên khi user load trang
var dataJson = localStorage.getItem("DATA_LOCAL");
if (dataJson != null) {
  // convert nvArr Json => array
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var nv = new NhanVien(
      item.taiKhoan,
      item.ten,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCoBan,
      item.chucVu,
      item.gioLam
    );
    nvArr.push(nv);
  }
  render(nvArr);
}

// Chức năng thêm nhân viên
document.getElementById("btnThemNV").onclick = function () {
  var nv = layThongTinTuForm();
  var isValid = validate(nv);

  if (isValid) {
    nvArr.push(nv);
    var dataJson = JSON.stringify(nvArr);
    localStorage.setItem("DATA_LOCAL", dataJson);
    render(nvArr);
  }
};
// Chức năng xóa nhân viên
function btnXoa(tk) {
  var viTri = nvArr.findIndex(function (item) {
    return item.taiKhoan == tk;
  });
  nvArr.splice(viTri, 1);
  render(nvArr);
}
// Chức năng sửa thông tin nhân viên
function btnSua(tk) {
  // tìm vị trí của object trong array
  var viTri = nvArr.findIndex(function (item) {
    return item.taiKhoan == tk;
  });
  if (viTri != -1) {
    // update vị trị => đưa thông tin lên modal
    showThongTinLenForm(nvArr[viTri]);
    document.getElementById("tknv").disabled = true;
  }
}
// Cập nhập thông tin
document.getElementById("btnCapNhat").onclick = function () {
  document.getElementById("tknv").disabled = false;
  var nv = layThongTinTuForm();

  // Validation
  var isValid = validate(nv);
 
  if (isValid) {
    var viTri = nvArr.findIndex(function (item) {
      return item.taiKhoan == nv.taiKhoan;
    });
    if (viTri != -1) {
      // document.getElementById("btnCapNhat").setAttribute("data-dismiss", "modal");
      nvArr[viTri] = nv;
      render(nvArr);
    }
  }
};

// Tìm nhân viên theo phân loại
document.getElementById("btnTimNV").onclick = function () {
  var inputUser = document.getElementById("searchNV").value.trim();
  var userSearch = nvArr.filter(function (item) {
    return item.loai.toUpperCase().includes(inputUser.toUpperCase());
  });
  render(userSearch);
};
